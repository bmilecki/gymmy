from rest_framework import viewsets, status, generics
from ..models import *
from .serializers import *
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from rest_framework.authtoken.models import Token
from .authentication import CustomModelBackend
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
import jwt
from django.conf import settings


class UserRegistrationViewSet(viewsets.ModelViewSet):
    queryset = UserData.objects.all
    serializer_class = UserDataSerializer

    @csrf_exempt
    def list(self, request):
        if request.method == 'POST':
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                self.perform_create(serializer)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
class LoginView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        user = get_object_or_404(UserData, username=request.data['username'])
        if not user.checkPassword(request.data['password']):
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)
        serializer = UserDataSerializer(user)
        print(user)
        return Response(serializer.data, status=status.HTTP_200_OK)

class UserUpdateAPIView(generics.UpdateAPIView):
    queryset=UserData.objects.all()
    serializer_class = UserDataSerializer
    lookup_field = 'id'
    
class ExerciseAPIView(generics.ListAPIView):
    queryset = ExerciseData.objects.all()
    serializer_class = ExerciseDataSerializer

    def post(self, request, *args, **kwargs):
        print(request)
        token = request.data.get('token')

        if token:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            user_id = payload['user_id']
            user = UserData.objects.get(pk=user_id)
            queryset = TrainingData.objects.filter(user=user)
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)


class ExerciseCreateAPIView(generics.CreateAPIView):
    queryset = ExerciseData.objects.all()
    serializer_class = ExerciseDataSerializer

class ExerciseUpdateAPIView(generics.UpdateAPIView):
    queryset = ExerciseData.objects.all()
    serializer_class = ExerciseDataSerializer
    lookup_field = 'id'

class ExerciseDeleteAPIView(generics.DestroyAPIView):
    queryset = ExerciseData.objects.all()
    lookup_field = 'id'
    
class TrainingAPIView(generics.ListAPIView):
    queryset = TrainingData.objects.all()
    serializer_class = TrainingDataSerializer

    def post(self, request, *args, **kwargs):
        print(request)
        token = request.data.get('token')

        if token:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            user_id = payload['user_id']
            user = UserData.objects.get(pk=user_id)
            queryset = TrainingData.objects.filter(user=user)
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

class TrainingAPIView2(generics.ListAPIView):
    queryset = TrainingData.objects.all()
    serializer_class = TrainingDataSerializer2

    def post(self, request, *args, **kwargs):
        print(request)
        token = request.data.get('token')

        if token:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            user_id = payload['user_id']
            user = UserData.objects.get(pk=user_id)
            queryset = TrainingData.objects.filter(user=user)
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

class TrainingCreateAPIView(generics.CreateAPIView):
    queryset = TrainingData.objects.all()
    serializer_class = TrainingDataSerializer

class TrainingUpdateAPIView(generics.UpdateAPIView):
    queryset = TrainingData.objects.all()
    serializer_class = TrainingDataSerializer
    lookup_field = 'id'

class TrainingDeleteAPIView(generics.DestroyAPIView):
    queryset = TrainingData.objects.all()
    lookup_field = 'id'

class TrainingExerciseAPIView(generics.ListAPIView):
    serializer_class = TrainingExerciseDataSerializer

    def get_queryset(self):
        # Assuming the training id is passed as a query parameter named 'training_id'
        training_id = self.request.query_params.get('training_id')
        # Filter the queryset based on the selected training id
        print(training_id)
        queryset = TrainingExerciseData.objects.filter(training=training_id)
        return queryset

class TrainingExerciseCreateAPIView(generics.CreateAPIView):
    queryset = TrainingExerciseData.objects.all()
    serializer_class = TrainingExerciseDataSerializer
    def post(self, request, *args, **kwargs):
        print(request)
        token = request.data.get('token')

        if token:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            user_id = payload['user_id']
            user = UserData.objects.get(pk=user_id)
            # Assuming you have a user associated with the token and you retrieve it somehow            
            if user:
                # Assuming you have 'training_id', 'exercise_id', 'sets', 'repetitons', and 'weight' in the request data
                training_id = request.data.get('training_id')
                exercise_id = request.data.get('exercise_id')
                sets = request.data.get('sets')
                repetitions = request.data.get('repetitions')
                weight = request.data.get('weight')

                if training_id and exercise_id and sets and repetitions and weight:
                    # Assuming TrainingData and ExerciseData are imported
                    try:
                        # Assuming TrainingData and ExerciseData are models
                        training = TrainingData.objects.get(id=training_id)
                        exercise = ExerciseData.objects.get(id=exercise_id)

                        # Create a new TrainingExerciseData record
                        training_exercise = TrainingExerciseData.objects.create(
                            training=training,
                            exercise=exercise,
                            sets=sets,
                            repetitons=repetitions,
                            weight=weight
                        )

                        # Serialize the newly created record
                        serializer = TrainingExerciseDataSerializer(training_exercise)

                        return Response(serializer.data, status=status.HTTP_201_CREATED)
                    except (TrainingData.DoesNotExist, ExerciseData.DoesNotExist) as e:
                        return Response({'error': 'Training or Exercise does not exist'}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({'error': 'Missing required fields'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({'error': 'Invalid token'}, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({'error': 'Token not provided'}, status=status.HTTP_400_BAD_REQUEST)

class TrainingExerciseUpdateAPIView(generics.UpdateAPIView):
    queryset = TrainingExerciseData.objects.all()
    serializer_class = TrainingExerciseDataSerializer
    lookup_field = 'id'

class TrainingExerciseDeleteAPIView(generics.DestroyAPIView):
    queryset = TrainingExerciseData.objects.all()
    lookup_field = 'id'

class ExerciseMuscleGroupAPIView(generics.ListAPIView):
    queryset = ExerciseMuscleGroupData.objects.all()
    serializer_class = ExerciseMuscleGroupDataSerializer

class ExerciseMuscleGroupCreateAPIView(generics.CreateAPIView):
    queryset = ExerciseMuscleGroupData.objects.all()
    serializer_class = ExerciseMuscleGroupDataSerializer

class ExerciseMuscleGroupUpdateAPIView(generics.UpdateAPIView):
    queryset = ExerciseMuscleGroupData.objects.all()
    serializer_class = ExerciseMuscleGroupDataSerializer
    lookup_field = 'id'

class ExerciseMuscleGroupDeleteAPIView(generics.DestroyAPIView):
    queryset = ExerciseMuscleGroupData.objects.all()
    lookup_field = 'id'

class MuscleGroupAPIView(generics.ListAPIView):
    queryset = MuscleGroupData.objects.all()
    serializer_class = MuscleGroupDataSerializer