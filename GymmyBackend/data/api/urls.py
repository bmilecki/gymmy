from rest_framework import routers
from .views import *
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view


router = routers.DefaultRouter()
router.register(r'register', UserRegistrationViewSet, basename="register")

urlpatterns = [
    path('', include(router.urls)),
    path('login/', LoginView.as_view(), name='login'),
    path('user/update/<int:id>', UserUpdateAPIView.as_view(), name='user-update'),
    path('exercises/', ExerciseAPIView.as_view(), name='exercise-list'),
    path('exercises/create', ExerciseCreateAPIView.as_view(), name='exercise-create'),
    path('exercises/update/<int:id>', ExerciseUpdateAPIView.as_view(), name='exercise-update'),
    path('exercises/delete/<int:id>', ExerciseDeleteAPIView.as_view(), name='exercise-delete'),
    path('training/', TrainingAPIView.as_view(), name='training-list'),
    path('training/create', TrainingCreateAPIView.as_view(), name='training-create'),
    path('training/update/<int:id>', TrainingUpdateAPIView.as_view(), name='training-update'),
    path('training/delete/<int:id>', TrainingDeleteAPIView.as_view(), name='training-delete'),
    path('trainingexercises/', TrainingExerciseAPIView.as_view(), name='trainingexercises-list'),
    path('trainingexercises/create', TrainingExerciseCreateAPIView.as_view(), name='trainingexercises-create'),
    path('trainingexercises/update/<int:id>', TrainingExerciseUpdateAPIView.as_view(), name='trainingexercises-update'),
    path('trainingexercises/delete/<int:id>', TrainingExerciseDeleteAPIView.as_view(), name='trainingexercises-delete'),
    path('exercisemuscle/', ExerciseMuscleGroupAPIView.as_view(), name='exercisemuscle-list'),
    path('exercisemuscle/create', ExerciseMuscleGroupCreateAPIView.as_view(), name='exercisemuscle-create'),
    path('exercisemuscle/update/<int:id>', ExerciseMuscleGroupUpdateAPIView.as_view(), name='exercisemuscle-update'),
    path('exercisemuscle/delete/<int:id>', ExerciseMuscleGroupDeleteAPIView.as_view(), name='exercisemuscle-delete'),
    path('musclegroup/', MuscleGroupAPIView.as_view(), name='musclegroup-list')
]