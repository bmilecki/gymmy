from rest_framework import serializers
from django.contrib.auth import authenticate
from ..models import *

class UserDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserData
        fields = ('id', 'username', 'password', 'weight', 'height')

class MuscleGroupDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = MuscleGroupData
        fields = ('id', 'name')

class ExerciseDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExerciseData
        fields = ('id', 'name', 'description', 'muscleGroup')

class ExerciseMuscleGroupDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExerciseMuscleGroupData
        fields = ('id', 'exercise', 'musclegroup')
    
class TrainingDataSerializer(serializers.ModelSerializer):
    exercise = serializers.PrimaryKeyRelatedField(many=True, queryset=ExerciseData.objects.all())

    class Meta:
        model = TrainingData
        fields = ('id', 'name', 'user', 'description', 'exercise')

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        exercises_data = ExerciseDataSerializer(instance.exercise, many=True).data
        print(representation['exercise'])
        for i in range(representation['exercise'].__len__()):
            pom = representation['exercise'][i]
            representation['exercise'][i] = {'id': pom, 'name': exercises_data[i]['name'], 'description': exercises_data[i]['description'], "musclegroup": exercises_data[i]['muscleGroup'] }

        return representation

class TrainingDataSerializer2(serializers.ModelSerializer):
    
    class Meta:
        model = TrainingData
        fields = ('id', 'name', 'user', 'description', 'exercise')

class TrainingExerciseDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingExerciseData
        fields = ('id', 'training', 'exercise', 'sets', 'repetitons', 'weight')

class PersonalAchievementsDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalAchievementsData
        fields = ('id', 'user', 'exercise', 'maxWeight')

class TrainingLogsDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainingLogsData
        fields = ('id', 'user', 'exercise', 'date', 'sets', 'repetitons', 'weight')
        