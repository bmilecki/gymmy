from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from .models import *
import collections


# Create your tests here.
class LoginSuccessfulTestCase(APITestCase):
    def testLogin(self):
        UserData.objects.create(username="a", password="a", weight="100.0", height="169")
        response = self.client.post("http://192.168.124.186:8000/api/login/", data={ 'username': 'a', 'password': 'a'}, headers={'Accept': '*/*', 'User-Agent': 'Thunder Client (https://www.thunderclient.com)'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class LoginUnsuccessfulTestCase(APITestCase):
    def testLogin2(self):
        UserData.objects.create(username="a", password="a", weight="100.0", height="169")
        response = self.client.post("http://192.168.124.186:8000/api/login/", data={ 'username': 'b', 'password': 'a'}, headers={'Accept': '*/*', 'User-Agent': 'Thunder Client (https://www.thunderclient.com)'})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class GetExercisesTestCase(APITestCase):
    def testExerciseGet(self):
        ExerciseData.objects.create(name="nogi", description="nogi to sie od chodzenia robią")
        response = self.client.get('http://192.168.124.186:8000/api/exercises/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        name = collections.OrderedDict(response.data[0]).popitem(last=False)
        self.assertEqual(name[1], 1)