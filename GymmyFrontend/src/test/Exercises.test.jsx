import React from 'react';
import { render, waitFor } from '@testing-library/react';
import Exercises from '../Exercises'; 
import { vi } from 'vitest';

global.fetch = vi.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve([{ id: 1, name: 'MuscleGroup1' }]), 
  })
);

global.fetch = vi.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve([{ id: 1, name: 'Exercise1' }]), 
    })
  );
  
  describe('Exercises', () => {
    it('fetches exercises and sets state', async () => {
      render(<Exercises />);
  
      await waitFor(() => {
        expect(global.fetch).toHaveBeenCalledWith('http://127.0.0.1:8000/api/exercises/', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });
      });
  
    });
  });

describe('Exercises', () => {
  it('fetches muscle groups and sets state', async () => {
    render(<Exercises />);

    await waitFor(() => {
      expect(global.fetch).toHaveBeenCalledWith('http://127.0.0.1:8000/api/musclegroup/', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
    });
  });
});
