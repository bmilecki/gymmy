import { fireEvent, render } from "@testing-library/react"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import { describe, expect, test, vi } from "vitest"
import Login from "../Login"

describe('<Login />', () => {
    test('App mounts properly', () => {
        const wrapper = render(
            <BrowserRouter>
             <Routes>
                <Route path='*' element={<Login/>}/>
             </Routes>s
            </BrowserRouter>
    )
        expect(wrapper).toBeTruthy()
    })
    
})