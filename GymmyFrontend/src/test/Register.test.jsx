import { describe, test, expect } from 'vitest'
import { render, screen } from '@testing-library/react'
import Register from '../Register'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

describe('<Register />', () => {
    test('App mounts properly', () => {
        const wrapper = render(
            <BrowserRouter>
             <Routes>
                <Route path='*' element={<Register/>}/>
             </Routes>s
            </BrowserRouter>
    )
        expect(wrapper).toBeTruthy()
    })
})