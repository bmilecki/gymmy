import { useContext } from "react";
import TokenContext from "./TokenConstext";
import NavBar from "./components/NavBar";
import Row from "react-bootstrap/esm/Row";


function Home(){
    const token = useContext(TokenContext)
    console.log(localStorage.getItem('token'))
    return(
        <div>
            <Row className="position-absolute top-0 start-0">
                <NavBar />
                <h1>Welcome!</h1>
            </Row>
        </div>
    )
}

export default Home;