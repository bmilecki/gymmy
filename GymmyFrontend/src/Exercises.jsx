import React, { useState, useEffect, Suspense } from 'react';
import { Card, Container, Row, Col, Pagination, Form, Modal, Button } from 'react-bootstrap';
import NavBar from "./components/NavBar";

const Exercises = () => {
  const serverIp = import.meta.env.VITE_SERVER_IP;
  const [exercises, setExercises] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [exercisesPerPage] = useState(3); // Liczba ćwiczeń na stronie
  const [muscleGroup, setMuscleGroup] = useState([]);
  const [selectedMuscleGroup, setSelectedMuscleGroup] = useState();
  const [show, setShow] = useState(false);
  const [training, setTraining] = useState([])
  const [selectedTraining, setSelectedTraining] = useState("")
  const [selectedExercise, setSelectedExercise] = useState("")
  const [sets, setSets] = useState("")
  const [reps, setReps] = useState("")
  const [weight, setWeight] = useState("")

  const handleClose = () => {
    setShow(false);
  }
  const handleShow = (exerciseId) => {
    setSelectedExercise(exerciseId)
    setShow(true);
  }

  const handleSelectTraining = (e) => {
    setSelectedTraining(e.target.value)
    console.log(selectedTraining)
  }

  const handleSelectExercise = (e) => {
    setSelectedExercise(e.target.value)
    console.log(selectedExercise)
  }

  const handleSetsChange = (e) => {
    setSets(e.target.value)
    console.log(sets)
  }

  const handleRepsChange = (e) => {
    setReps(e.target.value)
    console.log(reps)
  }

  const handleWeightChange = (e) => {
    setWeight(e.target.value)
    console.log(weight)
  }

  const handleSelectMuscleGroup = (e) => {
    setSelectedMuscleGroup(e.target.value)
  }

  const handleAddExerciseToTraining = () => {
    console.log(selectedTraining)
    console.log(selectedExercise)
    console.log(sets)
    console.log(reps)
    console.log(weight)

    fetch(`${serverIp}/api/trainingexercises/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: localStorage.getItem('token'),
                training_id: selectedTraining,
                exercise_id: selectedExercise,
                sets: sets,
                repetitions: reps,
                weight: weight
            }),
        })
        .then(response => console.log(response))
  }

  useEffect(() => {
    fetch(`${serverIp}/api/exercises/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      /*body: JSON.stringify({
        token: localStorage.getItem('token')
    }),*/
    })
    .then(response => response.json())
    .then(data => setExercises(data))
    .catch(error => console.error('Error fetching data:', error));

    fetch(`${serverIp}/api/musclegroup/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json())
    .then(data => setMuscleGroup(data))
    .catch(error => console.error('Error fetching data:', error))
  }, []);

  useEffect(() => {
    fetch(`${serverIp}/api/training/`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            token: localStorage.getItem('token')
        }),
    })
    .then((response) => response.json())
    .then(data => {
        setTraining(data)
    })
}, [])


  const indexOfLastExercise = currentPage * exercisesPerPage;
  const indexOfFirstExercise = indexOfLastExercise - exercisesPerPage;
  let currentExercises = selectedMuscleGroup ?
    exercises.filter(exercise => exercise.muscleGroup.includes(Number(selectedMuscleGroup))).slice(indexOfFirstExercise, indexOfLastExercise) :
    exercises.slice(indexOfFirstExercise, indexOfLastExercise);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
      <Container fluid className="position-absolute top-0 start-0">
        <NavBar />
        <Row>
          <Col xs={10}>
        <h1 className="text-center mb-4">Exercise list</h1>
          {currentExercises.map((exercise) => (
            <Col key={exercise.id}>
              <Card className="h-100">
                <Card.Body>
                  <Card.Title>{exercise.name}</Card.Title>
                  <Card.Text>{exercise.description}</Card.Text>
                  <Button variant='primary' onClick={() => handleShow(exercise.id)}>Add</Button>
                </Card.Body>
              </Card>
            </Col>
          ))}
          <Pagination className="justify-content-center mt-4">
            {[...Array(Math.ceil(exercises.length / exercisesPerPage)).keys()].map(number => (
              <Pagination.Item key={number + 1} onClick={() => paginate(number + 1)} active={number + 1 === currentPage}>
                {number + 1}
              </Pagination.Item>
            ))}
          </Pagination>
          </Col>
          <Col>
          <Form>
            <Form.Group controlId="muscleGroupSelect">
              <Form.Label>Choose muscle group</Form.Label>
              <Form.Select onChange={handleSelectMuscleGroup}>
                <option key="" value="">Choose...</option>
                {muscleGroup.map((group) => (
                  <option key={group.id} value={group.id}>
                    {group.name}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>
          </Form>
          </Col>
        </Row>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>
              Add and exercise to training
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Form onSubmit={handleAddExerciseToTraining}>
            <Col>
                <Row>
                    <Form.Group controlId='trainingSelect'>
                      <Form.Label>Choose training</Form.Label>
                      <Form.Select onChange={handleSelectTraining}>
                        <option key="" value="">Choose...</option>
                        {training.map((training) => (
                          <option key={training.id} value={training.id}>
                            {training.name}
                          </option>
                        ))}
                      </Form.Select>
                    </Form.Group>
                </Row>
                <Row>
                    <Form.Group controlId='exerciseSelect'>
                      <Form.Label>Choose exercise</Form.Label>
                      <Form.Select value={selectedExercise || ""} onChange={handleSelectExercise}>
                        <option key="" value="">Choose...</option>
                        {exercises.map((exercise) => (
                          <option key={exercise.id} value={exercise.id}>
                            {exercise.name}
                          </option>
                        ))}
                      </Form.Select>
                    </Form.Group>
                </Row>
                <Row>
                  <Form.Group controlId='setsInput'>
                    <Form.Label>Choose the number of sets</Form.Label>
                    <Form.Control type='text' placeholder='Sets' value={sets} onChange={handleSetsChange} />
                  </Form.Group>
                </Row>
                <Row>
                <Form.Group controlId='repsInput'>
                    <Form.Label>Choose the number of repetitions</Form.Label>
                    <Form.Control type='text' placeholder='Repetitions' value={reps} onChange={handleRepsChange} />
                  </Form.Group>
                </Row>
                <Row>
                <Form.Group controlId='weightInput'>
                    <Form.Label>Choose weight</Form.Label>
                    <Form.Control type='text' placeholder='Weight' value={weight} onChange={handleWeightChange} />
                  </Form.Group>
                </Row>
            </Col>
            <Button variant='primary' type='submit'>Submit</Button>
            </Form>
          </Modal.Body>
        </Modal>
      </Container>
      
  );
}

export default Exercises;
