import Container from "react-bootstrap/esm/Container";
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row'
import Button from "react-bootstrap/esm/Button";
import Nav from 'react-bootstrap/Nav';
import PasswordChecklist from "react-password-checklist"
import { useState } from 'react';
import { useNavigate } from "react-router-dom";


function Register(){
    const serverIp = import.meta.env.VITE_SERVER_IP;
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
	const [passwordConfirm, setPasswordConfirm] = useState("")
    const [validPassword, setValidPassword] = useState(false)
    const [weight, setWeight] = useState("")
    const [height, setHeight] = useState("")
    const [message, setMessage] = useState('');
    const navigate = useNavigate();

    const handleUsernameChange = (e) => {
        setUsername(e.target.value)
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
    }

    const handlePasswordConfirmChange = (e) => {
        setPasswordConfirm(e.target.value)
    }

    const handleWeightChange = (e) => {
        setWeight(e.target.value)
    }
    
    const handleHeightChange = (e) => {
        setHeight(e.target.value)
    }

    const handleRegister = async (e) => {
        e.preventDefault();
        if (!username || !password || !passwordConfirm || !weight || !height) {
            setMessage('All fields are required.');
            return;
        }

        if (!validPassword) {
            setMessage('Password does not meet the requirements.');
            return;
        }
        try {
            const response = await fetch(`${serverIp}/api/register/`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: username,
                    password: password,
                    weight: weight,
                    height: height
                }),
            });
            
            const data = await response.json();
            if (response.ok) {
                alert('Registration successful!');
                console.log(data);
                navigate("/?success=true")
            } else {
                if (response.status === 400 && data.username && data.username[0] === "user data with this username already exists.") {
                    setMessage('Username already exists. Please choose a different username.');
                } else {
                    setMessage('Registration failed: ' + JSON.stringify(data));
                }
            }
        } catch (error) {
            setMessage('An error occurred: ' + error.message);
        }
    }

    return(
        <Container fluid>
            <Row md={7}>
                <Form onSubmit={handleRegister} className="w-100">
                    <Col>
                        <Form.Label className="mb-3">Username</Form.Label>
                        <Form.Control type='text' placeholder='Username' value={username} onChange={handleUsernameChange}/>
                    </Col>
                    <Col>
                        <Form.Label className="mb-3">Password</Form.Label>
                        <Form.Control type='password' placeholder='Password' value={password} onChange={handlePasswordChange}/>
                    </Col>
                    <Col>
                        <Form.Label className="mb-3">Confirm password</Form.Label>
                        <Form.Control type='password' placeholder='Confirm password' value={passwordConfirm} onChange={handlePasswordConfirmChange}/>
                    </Col>
                    <PasswordChecklist
				        rules={["minLength","specialChar","number","capital","match"]}
				        minLength={5}
				        value={password}
				        valueAgain={passwordConfirm}
				        onChange={(isValid) => {setValidPassword(isValid)}}
			        />
                    <Col>
                        <Form.Label className="mb-3">Weight</Form.Label>
                        <Form.Control type='text' placeholder='Weight' value={weight} onChange={handleWeightChange}/>
                    </Col>
                    <Col>
                        <Form.Label className="mb-3">Height</Form.Label>
                        <Form.Control type='text' placeholder='Height' value={height} onChange={handleHeightChange}/>
                    </Col>
                    <Button className="mt-3" variant='primary' type='submit'>Submit</Button>
                    <Form >
                        {message && <p className="d-flex justify-content-center">{message}</p>}
                    </Form>
                </Form>
            </Row>
            <Row className="position-absolute bottom-0 start-50 translate-middle">
                <Nav
                  activeKey="/home"
                >
                    <Nav.Item>
                        <Nav.Link href="/">Login</Nav.Link>
                    </Nav.Item>
                </Nav>
            </Row>
        </Container>
    )
}

export default Register