import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row'
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import { useState } from 'react';
import { useNavigate  } from "react-router-dom";
import PropTypes from 'prop-types';

const Login = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const history = useNavigate()
    const serverIp = import.meta.env.VITE_SERVER_IP;

    const handleUsernameChange = (e) => {
        setUsername(e.target.value)
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
    }

    const handleLogin = async(e) => {
        e.preventDefault()
        fetch(`${serverIp}/api/login/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: username,
                password: password
            }),
        })
        .then((response) => response.json())
        .then(data => {
            console.log(data)
            fetch(`${serverIp}/api/token/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: username,
                    password: password
                }),
            })
            .then((response) => response.json())
            .then(data => {
                console.log(data)
                localStorage.setItem('token', data.access)
                localStorage.setItem('refresh', data.refresh)
                history('/home')
            })
        })
        .catch(error => {
            alert(error)
        })
    }

    return(
        <Container fluid>
            <Row md={7}>
            <Form onSubmit={handleLogin}>
            <Form.Label className='mb-3'>Login</Form.Label>
                <Form.Group className='mb-3' controlId='formUsername'>
                    <Col>
                        <Form.Label>Username</Form.Label>
                        <Form.Control type='text' placeholder='Username' value={username} onChange={handleUsernameChange}/>
                    </Col>
                </Form.Group>
                <Form.Group className='mb-3' controlId='formPasswd'>
                    <Col>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type='password' placeholder='Password' value={password} onChange={handlePasswordChange}/>
                        {/* <Form.Text className='text-muted'>a aa</Form.Text> */}
                    </Col>
                </Form.Group>
                <Button variant='primary' type='submit'>Submit</Button>
            </Form>
            </Row>
            <Row className="position-absolute bottom-0 start-50 translate-middle">
                <Nav
                  activeKey="/home"
                >
                    <Nav.Item>
                        <Nav.Link href="/register">Register</Nav.Link>
                    </Nav.Item>
                </Nav>
            </Row>
        </Container>
    )
}

Login.propTypes = {
    setContext: PropTypes.func,
}

export default Login;