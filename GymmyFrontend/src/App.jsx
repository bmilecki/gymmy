import './App.css'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from './Login';
import Home from './Home';
import Register from './Register';
import Training from './Training';
import Exercises from './Exercises';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path='/home' element={<Home />} />
        <Route path='/register' element={<Register />} />
        <Route path='/training' element={<Training />} />
        <Route path='/exercises' element={<Exercises />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
