import Row from "react-bootstrap/esm/Row";
import NavBar from "./components/NavBar";
import { useContext, useEffect, useState } from "react";
import TokenContext from "./TokenConstext";
import { Button } from "react-bootstrap";


const Training = () => {
    const serverIp = import.meta.env.VITE_SERVER_IP;
    const [trainings, setTraining] = useState()


    //fetch('http://127.0.0.1:8000/api/trainingexercises/delete/<int:id>')
    const handleDelete = (trainingId, exerciseId, index) => {
        console.log(trainingId)
        console.log(exerciseId)
        fetch(`${serverIp}/api/trainingexercises/?training_id=`+trainingId, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
    })
        .then((response) => response.json())
        .then((data) => {
            let indexCounter = 0
            data.forEach(trainingExercise => {
                if (trainingExercise.exercise == exerciseId && indexCounter == index){
                    fetch(`${serverIp}/api/trainingexercises/delete/` + trainingExercise.id, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                        }
                })}
                indexCounter += 1
            });
        })
    }

    useEffect(() => {
        fetch(`${serverIp}/api/training/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: localStorage.getItem('token')
            }),
        })
        .then((response) => response.json())
        .then(data => {
            setTraining(data)
        })
    }, [])
    console.log(trainings)
    return(
        <Row className="position-absolute top-0 start-0">
            <NavBar />
            <div>{trainings?.map(training => (
                <>
                    <h1>{training.name}</h1>
                    <h2>{training.description}</h2>
                    <div>{training.exercise?.map((exercise, index) => (
                        <>
                            <h3>{exercise.name}</h3>
                            <h3>{exercise.description}</h3>
                            <Button variant="danger" onClick={() => handleDelete(training.id, exercise.id, index)}>Delete</Button>
                        </>
                    ))}</div>
                </>
            ))}</div>
        </Row>
    )
}

export default Training;