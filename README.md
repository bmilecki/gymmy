# Gymmy

## Table of Contents

1. [Introduction](#introduction)
2. [Technologies Used](#technologies-used)
3. [Database Diagram](#database-diagram)
4. [Use Cases](#use-cases)
5. [Authors](#authors)

## Introduction

Gymmy is a web app to help organize and plan trainings in the form of lists.

## Technologies Used

### React with JavaScript

React is a popular JavaScript library for building user interfaces, particularly single-page applications where you need a fast and interactive user experience. It allows developers to create large web applications that can update and render efficiently in response to data changes. The UI has been created with the help of React Bootstrap

### Django with Python

Django is a high-level Python web framework that encourages rapid development and clean, pragmatic design. It takes care of much of the hassle of web development, allowing developers to focus on writing their app without needing to reinvent the wheel.
Django is used to create the server side of the application.

### Vite

Vite is a build tool that aims to provide a faster and leaner development experience for modern web projects. It consists of two major parts: a development server that provides rich feature enhancements and a build command that bundles your code with Rollup.

### Docker

Docker is a platform designed to help developers build, share, and run modern applications. Using containers, it allows you to package an application with all of its dependencies into a standardized unit for software development.

### Netlify

Netlify is a web development platform that multiplies productivity by unifying the modern decoupled web. It provides hosting for web applications and static websites, with features like continuous deployment, a global content delivery network, and an intuitive workflow for managing deployments. 
It has been used to deploy the front-end part of the application.

### Render

Render is a unified cloud platform that allows you to build and run all your applications with ease. It provides various services such as web applications, databases, static sites, and more, with automatic scaling and zero downtime deploys.
It has been used to deploy the back-end part of the application.

## Database Diagram

![db_diagram](/img/database_diagram.png)

## Use Cases

![use_cases](/img/use_cases_diagram.png)

## Authors

Bartłomiej Milecki  
Rafał Ciupek